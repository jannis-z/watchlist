﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace M120Projekt
{
    /// <summary>
    /// Interaktionslogik für validatingInput.xaml
    /// </summary>
    public partial class validatingInput : UserControl
    {



        public Boolean IsRequired
        {
            get { return (Boolean)GetValue(IsRequiredProperty); }
            set { SetValue(IsRequiredProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRequired.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRequiredProperty =
            DependencyProperty.Register("IsRequired", typeof(Boolean), typeof(validatingInput), new PropertyMetadata(false));



        public Boolean IsValid
        {
            get { return (Boolean)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsValid.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsValidProperty =
            DependencyProperty.Register("IsValid", typeof(Boolean), typeof(validatingInput), new PropertyMetadata(false));




        public String ErrorMessage
        {
            get { return (String)GetValue(ErrorMessageProperty); }
            set { SetValue(ErrorMessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ErrorMessage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ErrorMessageProperty =
            DependencyProperty.Register("ErrorMessage", typeof(String), typeof(validatingInput), new PropertyMetadata(""));



        public String RegexValue
        {
            get { return (String)GetValue(RegexValueProperty); }
            set { SetValue(RegexValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RegexValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RegexValueProperty =
            DependencyProperty.Register("RegexValue", typeof(String), typeof(validatingInput), new PropertyMetadata(""));


        public validatingInput()
        {
            InitializeComponent();
        }

        private void Textinput_title_TextChanged(object sender, TextChangedEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(RegexValue);
            System.Text.RegularExpressions.Match match = regex.Match(textinput_title.Text);
            if (!IsRequired && textinput_title.Text.Length == 0)
            {
                SetValue(IsValidProperty, true);
                err_message.Content = "";
            }
            else if (match.Success)
            {
                SetValue(IsValidProperty, true);
                err_message.Content = "";
            } else
            {
                SetValue(IsValidProperty, false);
                err_message.Content = ErrorMessage;
            }
        }
    }
}
