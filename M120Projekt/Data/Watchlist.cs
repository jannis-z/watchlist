﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace M120Projekt.Data
{
    public class Watchlist
    {
        #region Datenbankschicht
        [Key]
        public Int64 WatchlistId { get; set; }
        [Required]
        public String Title { get; set; }
        public String MainActor { get; set; }
        public String Director { get; set; }
        [Required]
        public Int16 Ranking { get; set; }
        public DateTime ReleaseDate { get; set; }
        [Required]
        public Boolean IsReleased { get; set; }
        [Required]
        public String Platform { get; set; }
        public String Type { get; set; }
        #endregion
        #region Applikationsschicht
        public Watchlist() { }
        [NotMapped]
        public String BerechnetesAttribut
        {
            get
            {
                return "Im Getter kann Code eingefügt werden für berechnete Attribute";
            }
        }
        public static IEnumerable<Data.Watchlist> LesenAlle()
        {
            return (from record in Data.Global.context.Watchlist select record);
        }
        public static Data.Watchlist LesenID(Int64 klasseAId)
        {
            return (from record in Data.Global.context.Watchlist where record.WatchlistId == klasseAId select record).FirstOrDefault();
        }
        public static IEnumerable<Data.Watchlist> LesenAttributGleich(String suchbegriff)
        {
            return (from record in Data.Global.context.Watchlist where record.Title == suchbegriff select record);
        }
        public static IEnumerable<Data.Watchlist> LesenAttributWie(String suchbegriff)
        {
            return (from record in Data.Global.context.Watchlist where record.Title.Contains(suchbegriff) select record);
        }
        public Int64 Erstellen()
        {
            if (this.Title == null || this.Title == "") this.Title = "leer";
            if (this.ReleaseDate == null) this.ReleaseDate = DateTime.MinValue;
            Data.Global.context.Watchlist.Add(this);
            Data.Global.context.SaveChanges();
            return this.WatchlistId;
        }
        public Int64 Aktualisieren()
        {
            Data.Global.context.Entry(this).State = System.Data.Entity.EntityState.Modified;
            Data.Global.context.SaveChanges();
            return this.WatchlistId;
        }
        public void Loeschen()
        {
            Data.Global.context.Entry(this).State = System.Data.Entity.EntityState.Deleted;
            Data.Global.context.SaveChanges();
        }
        public override string ToString()
        {
            return WatchlistId.ToString(); // Für bessere Coded UI Test Erkennung
        }
        #endregion
    }
}
