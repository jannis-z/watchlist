﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace M120Projekt
{
    /// <summary>
    /// Interaktionslogik für add.xaml
    /// </summary>
    public partial class add : Window
    {
        public Boolean dateValid = false;
        public Boolean platformValid = false;
        public Boolean titleValid = false;
        public Boolean mainActorValid = false;
        public Boolean directorValid = false;
        public Boolean rankingValid = false;
        public String modeValue;
        public Int64 entryIdValue;
        public add(Int64 entryId, String mode)
        {
            InitializeComponent();
            modeValue = mode;
            entryIdValue = entryId;
            if (mode == "edit")
            {
                button_add_entry.Content = "Edit";
            } else
            {
                button_add_entry.Content = "Add";
            }
            Data.Watchlist entry = Data.Watchlist.LesenID(entryId);
            if (mode == "edit")
            {
                label_add_view_title.Content = "Edit Movie or TV Series";
                textbox_title.Text = entry.Title;
                textbox_main_actor.Text = entry.MainActor;
                textbox_director.Text = entry.Director;
                textbox_ranking.Text = entry.Ranking.ToString();
                checkbox_released.IsChecked = entry.IsReleased;
                datepicker_release_date.SelectedDate = entry.ReleaseDate;
                combobox_platform.Text = entry.Platform;
                combobox_type.Text = entry.Type;
            }
            
        }

        private void Button_add_entry_Click(object sender, RoutedEventArgs e)
        {
            if (dateValid && platformValid && titleValid && mainActorValid && directorValid && rankingValid)
            {
                if (modeValue == "edit")
                {
                    Data.Watchlist newEntry = Data.Watchlist.LesenID(entryIdValue);
                    newEntry.Title = textbox_title.Text;
                    newEntry.MainActor = textbox_main_actor.Text;
                    newEntry.Director = textbox_director.Text;
                    newEntry.Ranking = short.Parse(textbox_ranking.Text);
                    newEntry.IsReleased = (checkbox_released.IsChecked ?? false);
                    newEntry.ReleaseDate = (datepicker_release_date.SelectedDate ?? new DateTime());
                    newEntry.Platform = combobox_platform.Text;
                    newEntry.Type = combobox_type.Text;
                    newEntry.Aktualisieren();
                } else
                {
                    Data.Watchlist newEntry = new Data.Watchlist();
                    newEntry.Title = textbox_title.Text;
                    newEntry.MainActor = textbox_main_actor.Text;
                    newEntry.Director = textbox_director.Text;
                    newEntry.Ranking = short.Parse(textbox_ranking.Text);
                    newEntry.IsReleased = (checkbox_released.IsChecked ?? false);
                    newEntry.ReleaseDate = (datepicker_release_date.SelectedDate ?? new DateTime());
                    newEntry.Platform = combobox_platform.Text;
                    newEntry.Type = combobox_type.Text;
                    Int64 klasseA1Id = newEntry.Erstellen();
                }
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            } else
            {
                if (!dateValid) { err_message_date.Content = "Please enter a valid date";  }
                if (!platformValid) { err_message_platform.Content = "The platform is required";  }
                if (!titleValid) { err_message_title.Content = "Title: min 2, max 50 characters, required"; }
                if (!mainActorValid) { err_message_main_actor.Content = "Main actor: min 2, max 50 characters"; }
                if (!directorValid) { err_message_director.Content = "Director: min 2, max 50 characters, required"; }
                if (!rankingValid) { err_message_ranking.Content = "Ranking: digits between 1 and 10, required"; }
                err_message_form.Content = "Invalid";
            }
        }

        private void Datepicker_release_date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datepicker_release_date.Text != "")
            {
                dateValid = true;
                err_message_date.Content = "";
            }
            else
            {
                dateValid = false;
                err_message_date.Content = "Please enter a valid date";
            }
        }

        private void Combobox_platform_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (combobox_platform.Text != null)
            {
                platformValid = true;
                err_message_platform.Content = "";
            } else
            {
                platformValid = false;
                err_message_platform.Content = "The platform is required";
            }
        }

        private void Textbox_title_TextChanged(object sender, TextChangedEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[a-zA-Z\\s0-9]{2,50}$");
            System.Text.RegularExpressions.Match match = regex.Match(textbox_title.Text);

            if (textbox_title.Text.Length != 0 && match.Success)
            {
                titleValid = true;
                err_message_title.Content = "";
            } else
            {
                titleValid = false;
                err_message_title.Content = "Title: min 2, max 50 characters, required";
            }
        }

        private void Textbox_main_actor_TextChanged(object sender, TextChangedEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[a-zA-Z\\s0-9]{2,50}$");
            System.Text.RegularExpressions.Match match = regex.Match(textbox_main_actor.Text);

            if ((textbox_main_actor.Text.Length != 0 && match.Success) || textbox_main_actor.Text.Length == 0)
            {
                mainActorValid = true;
                err_message_main_actor.Content = "";
            }
            else
            {
                mainActorValid = false;
                err_message_main_actor.Content = "Main actor: min 2, max 50 characters";
            }
        }

        private void Textbox_director_TextChanged(object sender, TextChangedEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[a-zA-Z\\s0-9]{2,50}$");
            System.Text.RegularExpressions.Match match = regex.Match(textbox_director.Text);

            if (textbox_director.Text.Length != 0 && match.Success)
            {
                directorValid = true;
                err_message_director.Content = "";
            }
            else
            {
                directorValid = false;
                err_message_director.Content = "Director: min 2, max 50 characters, required";
            }
        }

        private void Textbox_ranking_TextChanged(object sender, TextChangedEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^([1-9]|10)$");
            System.Text.RegularExpressions.Match match = regex.Match(textbox_ranking.Text);

            if (textbox_ranking.Text.Length != 0 && match.Success)
            {
                rankingValid = true;
                err_message_ranking.Content = "";
            }
            else
            {
                rankingValid = false;
                err_message_ranking.Content = "Ranking: digits between 1 and 10, required";
            }
        }

        private void Button_cancel_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}
