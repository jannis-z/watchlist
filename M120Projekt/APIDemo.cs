﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M120Projekt
{
    static class APIDemo
    {
        #region Watchlist
        // Create
        public static void DemoACreate()
        {
            Debug.Print("--- DemoACreate ---");
            // KlasseA
            Data.Watchlist klasseA1 = new Data.Watchlist();
            klasseA1.Title = "Star Wars 1";
            klasseA1.ReleaseDate = new DateTime(1999, 5, 16);
            klasseA1.MainActor = "Hayden Christensen";
            klasseA1.Director = "George Lucas";
            klasseA1.Ranking = 7;
            klasseA1.IsReleased = true;
            klasseA1.Platform = "Cinema";
            klasseA1.Type = "Movie";
            Int64 klasseA1Id = klasseA1.Erstellen();
            Debug.Print("Artikel erstellt mit Id:" + klasseA1Id);
        }
        public static void DemoACreateKurz()
        {
            Data.Watchlist klasseA2 = new Data.Watchlist { Title = "Avatar", IsReleased = true, ReleaseDate = new DateTime(2009, 12, 10), MainActor = "Sam Worthington", Director = "James Cameron", Ranking = 8, Platform = "Cinema", Type = "Movie" };
            Int64 klasseA2Id = klasseA2.Erstellen();
            Debug.Print("Artikel erstellt mit Id:" + klasseA2Id);
        }

        // Read
        public static void DemoARead()
        {
            Debug.Print("--- DemoARead ---");
            // Demo liest alle
            foreach (Data.Watchlist klasseA in Data.Watchlist.LesenAlle())
            {
                Debug.Print("Artikel Id:" + klasseA.WatchlistId + " Name:" + klasseA.Title);
            }
        }
        // Update
        public static void DemoAUpdate()
        {
            Debug.Print("--- DemoAUpdate ---");
            // KlasseA ändert Attribute
            Data.Watchlist klasseA1 = Data.Watchlist.LesenID(1);
            klasseA1.Title = "Artikel 1 nach Update";
            klasseA1.Aktualisieren();
        }
        // Delete
        public static void DemoADelete()
        {
            Debug.Print("--- DemoADelete ---");
            Data.Watchlist.LesenID(1).Loeschen();
            Debug.Print("Artikel mit Id 1 gelöscht");
        }
        #endregion
    }
}
