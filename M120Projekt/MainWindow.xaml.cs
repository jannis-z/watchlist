﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;

namespace M120Projekt
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Data.Watchlist> watchList;
        public MainWindow()
        {
            InitializeComponent();
            // Wichtig!
            Data.Global.context = new Data.Context();
            // Aufruf diverse APIDemo Methoden
            APIDemo.DemoACreate();
            APIDemo.DemoACreateKurz();
            //APIDemo.DemoARead();
            //APIDemo.DemoAUpdate();
            //APIDemo.DemoARead();
            //APIDemo.DemoADelete();

            watchList = Data.Watchlist.LesenAlle().ToList();
            this.DataContext = watchList;
            datagrid_watchlist.CanUserAddRows = false;
            datagrid_watchlist.CanUserSortColumns = true;
            datagrid_watchlist.IsReadOnly = true;
            datagrid_watchlist.SelectionMode = DataGridSelectionMode.Single;

            // Data.Watchlist entry = Data.Watchlist.LesenID(1);
            // if (entry != null)
            // {
            //    title_value.Content = entry.Title;
            //    release_date_value.Content = entry.ReleaseDate;
            //    platform_value.Content = entry.Platform;
            //    type_value.Content = entry.Type;
            //    main_actor_value.Content = entry.MainActor;
            //    director_value.Content = entry.Director;
            //    ranking_value.Content = entry.Ranking;
            // }
        }

        private void Button_page_add_Click(object sender, RoutedEventArgs e)
        {
            add addWindow = new add(0, "add");
            addWindow.Show();
            this.Close();
        }

        private void Button_page_delete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Data.Watchlist wl = (Data.Watchlist)datagrid_watchlist.SelectedItem;
                if (Data.Watchlist.LesenID(wl.WatchlistId) != null)
                {
                    Data.Watchlist.LesenID(wl.WatchlistId).Loeschen();
                    datagrid_watchlist.Items.Refresh();
                    watchList = null;
                    watchList = Data.Watchlist.LesenAlle().ToList();
                    this.DataContext = watchList;
                }
            }
        }

        private void Button_page_edit_Click(object sender, RoutedEventArgs e)
        {
            Data.Watchlist wl = (Data.Watchlist)datagrid_watchlist.SelectedItem;
            if (Data.Watchlist.LesenID(wl.WatchlistId) != null)
            {
               add addWindow = new add(wl.WatchlistId, "edit");
               addWindow.Show();
               this.Close();
             }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (textbox_search.Text != "")
            {
                watchList = Data.Watchlist.LesenAlle().ToList();
                watchList = (from items in watchList where items.Title.ToLower().StartsWith(textbox_search.Text.ToLower()) select items).ToList();
                this.DataContext = watchList;
            }
        }

        private void Textbox_search_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (textbox_search.Text != "")
            {
                watchList = Data.Watchlist.LesenAlle().ToList();
                watchList = (from items in watchList where items.Title.ToLower().StartsWith(textbox_search.Text.ToLower()) select items).ToList();
                this.DataContext = watchList;
            } else
            {
                watchList = Data.Watchlist.LesenAlle().ToList();
                this.DataContext = watchList;
            }
        }
    }
}
